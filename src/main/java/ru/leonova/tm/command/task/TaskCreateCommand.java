package ru.leonova.tm.command.task;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.enumerated.RoleType;

import java.util.Collection;
import java.util.Scanner;

public final class TaskCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "create-t";
    }

    @Override
    public String getDescription() {
        return "Create task";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }

    @Override
    public void execute() {
        if (serviceLocator.getProjectService().isEmptyProjectList()) return;
        System.out.println("[CREATE TASK]\nList projects:");
        Collection<Project> projectCollection = serviceLocator.getProjectService().getList();
        int i=0;
        for (Project project:projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getPrID() + ", NAME: " + project.getName());
        }
        System.out.println("\nSELECT ID PROJECT: ");
        String id = getScanner().nextLine();
        System.out.println("Enter name task: ");
        Scanner input = new Scanner(System.in);
        String name = input.nextLine();
        Task t = new Task(name, id);
        serviceLocator.getTaskService().create(t);
        System.out.println("Task created");
    }
}

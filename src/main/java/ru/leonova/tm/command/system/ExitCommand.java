package ru.leonova.tm.command.system;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.enumerated.RoleType;

public final class ExitCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit the application";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}


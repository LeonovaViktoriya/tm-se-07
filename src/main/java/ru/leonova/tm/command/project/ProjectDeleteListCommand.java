package ru.leonova.tm.command.project;

import ru.leonova.tm.command.AbstractCommand;

public final class ProjectDeleteListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-all-p";
    }

    @Override
    public String getDescription() {
        return "Delete list projects";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        System.out.println("["+getDescription().toUpperCase()+"]");
        if (!serviceLocator.getUserService().isAuth()){
            System.out.println("You are not authorized");
            return;
        }
        serviceLocator.getTaskService().deleteAllTask();
        serviceLocator.getProjectService().deleteAllProject();
        System.out.println("All projects remove");
    }
}

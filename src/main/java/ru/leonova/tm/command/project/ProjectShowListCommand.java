package ru.leonova.tm.command.project;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;

import java.util.Collection;

public final class ProjectShowListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "list-p";
    }

    @Override
    public String getDescription() {
        return "Show project list";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        if (serviceLocator.getProjectService().isEmptyProjectList()) return;
        Collection<Project> projectCollection = serviceLocator.getProjectService().getList();
        int i=0;
        for (Project project:projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getPrID() + ", NAME: " + project.getName());
        }
    }
}

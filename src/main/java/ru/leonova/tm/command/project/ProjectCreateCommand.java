package ru.leonova.tm.command.project;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;

public final class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "create-p";
    }

    @Override
    public String getDescription() {
        return "Create project";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        if (!serviceLocator.getUserService().isAuth()){
            System.out.println("You are not authorized");
            return;
        }
        System.out.print("[CREATE PROJECT]\nINTER NAME: \n");
        Project project = new Project(getScanner().nextLine());
        serviceLocator.getProjectService().create(project);
//        serviceLocator.getUserService().getCurrentUser().setProjectId(project.getPrID());
        System.out.println("Project created");
    }
}

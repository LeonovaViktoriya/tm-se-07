package ru.leonova.tm.command.project;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;

import java.util.Collection;

public final class ProjectDeleteCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-p";
    }

    @Override
    public String getDescription() {
        return "Delete project with all his tasks";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        if (!serviceLocator.getUserService().isAuth()){
            System.out.println("You are not authorized");
            return;
        }
        if (serviceLocator.getProjectService().isEmptyProjectList()) return;
        System.out.println("[DELETE PROJECT BY ID]\nList projects:");
        Collection<Project> projectCollection = serviceLocator.getProjectService().getList();
        int i=0;
        for (Project project:projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getPrID() + ", NAME: " + project.getName());
        }
        System.out.println("Enter id project:");
        String id = getScanner().nextLine();
        if (id == null || id.isEmpty()) return;
        serviceLocator.getTaskService().deleteTasksByIdProject(id);
        serviceLocator.getProjectService().deleteProject(id);
        System.out.println("Project with his tasks are removed");
    }
}

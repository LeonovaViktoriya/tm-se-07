package ru.leonova.tm.command.user;

import ru.leonova.tm.command.AbstractCommand;

public final class UserSessionEndCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "session-end";
    }

    @Override
    public String getDescription() {
        return "Завершение сеанса пользователя";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        System.out.println("["+getDescription().toUpperCase()+"]");
//        serviceLocator.getUserService().getCurrentUser().getProjectId();
        serviceLocator.getUserService().setCurrentUser(null);
    }
}

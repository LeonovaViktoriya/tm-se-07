package ru.leonova.tm.command.user;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;

import java.util.Collection;

public final class UserUpdateRoleCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "up-role";
    }

    @Override
    public String getDescription() {
        return "Admin can edit role user to admin";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() {
        if (!serviceLocator.getUserService().getCurrentUser().getRoleType().equals(RoleType.ADMIN.getRole())) {
            System.out.println("This option available just for admin");
            return;
        }
        System.out.println("[" + getDescription().toUpperCase() + "]");
        System.out.println("[List user:]");
        Collection<User> userCollection = serviceLocator.getUserService().getList();
        int i = 0;
        for (User user : userCollection) {
            i++;
            System.out.println(i+". Login: " + user.getLogin() + ", Password: " + user.getPassword() + ", Id: " + user.getUserId() + ", Role type: " + user.getRoleType());
        }
        System.out.println("Enter id user");
        String userId = getScanner().nextLine();
        User user = serviceLocator.getUserService().getById(userId);
        if (user == null) {
            System.out.println("User with this id not found");
        } else {
            user.setRoleType(RoleType.ADMIN.getRole());
            System.out.println("Role user was update to admin");
        }

    }
}

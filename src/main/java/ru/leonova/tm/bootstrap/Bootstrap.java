package ru.leonova.tm.bootstrap;

import ru.leonova.tm.api.ServiceLocator;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.api.repository.ITaskRepository;
import ru.leonova.tm.api.repository.IUserRepository;
import ru.leonova.tm.api.service.IProjectService;
import ru.leonova.tm.api.service.ITaskService;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.repository.ProjectRepository;
import ru.leonova.tm.repository.TaskRepository;
import ru.leonova.tm.repository.UserRepository;
import ru.leonova.tm.api.service.IUserService;
import ru.leonova.tm.service.ProjectService;
import ru.leonova.tm.service.TaskService;
import ru.leonova.tm.service.UserService;

import java.util.*;

public final class Bootstrap implements ServiceLocator {

    private final Scanner scanner = new Scanner(System.in);
    private final IProjectRepository IProjectRepository = new ProjectRepository();
    private final ITaskRepository ITaskRepository = new TaskRepository();
    private final IUserRepository IUserRepository = new UserRepository();
    private final IProjectService IProjectService = new ProjectService(IProjectRepository);
    private final ITaskService ITaskService = new TaskService(ITaskRepository);
    private final IUserService IUserService = new UserService(IUserRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public Bootstrap() {
    }

    @Override
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @Override
    public IProjectService getProjectService() {
        return IProjectService;
    }

    @Override
    public IUserService getUserService() {
        return IUserService;
    }

    @Override
    public ITaskService getTaskService() {
        return ITaskService;
    }

    private void registry(AbstractCommand command) throws Exception {
        String cliCommand = command.getName();
        String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty()) throw new Exception("It is not enumerated");
        if (cliDescription == null || cliDescription.isEmpty()) throw new Exception("Not description");
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    private boolean isCorrectCommand(String command) {
        for (AbstractCommand c:commands.values()) {
            if (c.getName().equals(command)) {
                return true;
            }
        }
        return false;
    }

    private void registry(Class... classes) throws Exception {
        for (Class clazz : classes) {
            if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
            Object command = clazz.newInstance();
            AbstractCommand abstractCommand = (AbstractCommand) command;
            registry(abstractCommand);
        }
    }

    public void init(Class... classes) throws Exception {
        if (classes == null || classes.length == 0) throw new Exception("Empty list");
        registry(classes);
        initAdmin();
        start();
    }

    private void initAdmin() {
        try {
            User admin = new User("admin", IUserService.md5Apache("admin"));
            admin.setRoleType(RoleType.ADMIN.getRole());
            IUserService.adminRegistration(admin);
        } catch (Exception e) {
            System.out.println("Something went wrong in initAdmin!");
            e.printStackTrace();
        }
    }

    private void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command;
        do {
            command = scanner.nextLine();
            execute(command);
        } while (!command.equals("exit"));
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty() || !isCorrectCommand(command)) return;
        final AbstractCommand abstractCommand = commands.get(command);
        final boolean secureCheck = !abstractCommand.secure() || (abstractCommand.secure() && IUserService.isAuth());
        if (secureCheck || IUserService.getCurrentUser() != null) {
            abstractCommand.execute();
        } else {
            System.out.println("Log in for this command");
        }

    }

}


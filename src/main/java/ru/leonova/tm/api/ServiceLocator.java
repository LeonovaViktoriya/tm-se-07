package ru.leonova.tm.api;

import ru.leonova.tm.api.service.IProjectService;
import ru.leonova.tm.api.service.ITaskService;
import ru.leonova.tm.api.service.IUserService;
import ru.leonova.tm.command.AbstractCommand;

import java.util.List;

public interface ServiceLocator {
    List<AbstractCommand> getCommands();

    IProjectService getProjectService();

    IUserService getUserService();

    ITaskService getTaskService();
}

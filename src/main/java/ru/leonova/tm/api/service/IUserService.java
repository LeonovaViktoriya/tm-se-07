package ru.leonova.tm.api.service;

import ru.leonova.tm.entity.User;

import java.util.Collection;

public interface IUserService {
    User getCurrentUser();

    void setCurrentUser(User currentUser);

    void create(User user);

    User authorizationUser(String login, String password);

    boolean isAuth();

    User getById(String userId);

    String md5Apache(String password);

    void adminRegistration(User admin);

    Collection<User> getList();
}

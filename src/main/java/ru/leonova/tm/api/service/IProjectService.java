package ru.leonova.tm.api.service;

import ru.leonova.tm.entity.Project;

import java.util.Collection;

public interface IProjectService {
    Collection<Project> getList();

    void create(Project project);

    void updateProject(String projectId, String name);

    boolean isEmptyProjectList();

    Project getById(String id);

    void deleteProject(String projectId);

    void deleteAllProject();
}

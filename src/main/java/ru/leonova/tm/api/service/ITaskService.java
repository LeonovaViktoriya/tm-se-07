package ru.leonova.tm.api.service;

import ru.leonova.tm.entity.Task;

import java.util.Collection;

public interface ITaskService {

    void create(Task t);

    Collection<Task> getList();

    void updateTaskName(String taskId, String taskName);

    void deleteTasksByIdProject(String projectId);

    boolean isEmptyTaskList();

    void deleteTask(String taskId);

    Task getById(String id);

    void deleteAllTask();
}

package ru.leonova.tm.api.repository;

import ru.leonova.tm.entity.Task;

import java.util.Collection;

public interface ITaskRepository {
    void persist(Task task);

    void merge(Task task);

    Collection<Task> findAll();

    Task findOne(String taskId);

    void remove(Task t);

    void removeAll();
}

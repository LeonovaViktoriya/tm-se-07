package ru.leonova.tm.repository;

import ru.leonova.tm.entity.User;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class UserRepository extends AbstractRepository<User> implements ru.leonova.tm.api.repository.IUserRepository {

    final private Map<String, User> userMap = new LinkedHashMap();

    @Override
    public void persist(User user) {
        userMap.put(user.getUserId(), user);
    }

    @Override
    void merge(User user) {
        for (User u : userMap.values()) {
            if(u.getLogin().equals(user.getLogin())){
                u.setLogin(user.getLogin());
            }else {
                persist(user);
            }
        }
    }

    @Override
    public User findOne(String userId) {
        return userMap.get(userId);
    }

    @Override
    public Collection<User> findAll() {
        return userMap.values();
    }

    @Override
    public void remove(User user) {
        userMap.remove(user.getUserId());
    }

    @Override
    public void removeAll() {
        userMap.clear();
    }

}

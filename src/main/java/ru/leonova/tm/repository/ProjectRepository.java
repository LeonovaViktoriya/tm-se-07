package ru.leonova.tm.repository;

import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.entity.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    private final Map<String, Project> projectMap = new LinkedHashMap();

    @Override
    public void persist(Project p) {
        projectMap.put(p.getPrID(), p);
    }

    @Override
    public Collection<Project> findAll() {
        return projectMap.values();
    }

    @Override
    public Project findOne(String id) {
        return projectMap.get(id);
    }

    private void updateProjectName(String projectId, String name) {
        Project project = projectMap.get(projectId);
        if(project!=null) project.setName(name);
    }

    @Override
    public void merge(Project p) {
        for (Project project : findAll()) {
            if (project.getPrID().equals(p.getPrID())) {
                updateProjectName(project.getPrID(), p.getName());
            } else {
                persist(p);
            }
        }
    }

    @Override
    public void remove(Project p) {
        projectMap.remove(p.getPrID());
    }

    @Override
    public void removeAll() {
        projectMap.clear();
    }

}

package ru.leonova.tm.repository;

abstract class AbstractRepository<E> {

    abstract void persist(E e);
    abstract void merge(E e);
}

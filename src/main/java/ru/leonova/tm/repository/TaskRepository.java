package ru.leonova.tm.repository;

import ru.leonova.tm.entity.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class TaskRepository extends AbstractRepository<Task> implements ru.leonova.tm.api.repository.ITaskRepository {

    final private Map<String, Task> taskMap = new LinkedHashMap();

    @Override
    public void persist(Task task) {
        taskMap.put(task.getTaskId(), task);
    }

    @Override
    public void merge(Task task){
        for (Task t : taskMap.values()) {
            if(t.getTaskId().equals(task.getTaskId())){
                t.setName(task.getName());
            }else {
                persist(task);
            }
        }
    }

    @Override
    public Collection<Task> findAll(){
        return taskMap.values();
    }

    @Override
    public Task findOne(String taskId) {
        return taskMap.get(taskId);
    }

    @Override
    public void remove(Task t) {
        taskMap.remove(t.getTaskId());
    }

    @Override
    public void removeAll() {
        taskMap.clear();
    }

}


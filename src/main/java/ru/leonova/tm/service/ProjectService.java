package ru.leonova.tm.service;

import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.api.service.IProjectService;
import ru.leonova.tm.entity.Project;

import java.util.Collection;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    final private IProjectRepository IProjectRepository;

    public ProjectService(IProjectRepository IProjectRepository) {
        this.IProjectRepository = IProjectRepository;
    }

    @Override
    public Collection<Project> getList() {
        return IProjectRepository.findAll();
    }

    @Override
    public void create(Project project) {
        if (project == null) return;
        if (IProjectRepository.findAll().isEmpty()) {
            IProjectRepository.persist(project);
            return;
        }
        IProjectRepository.merge(project);
    }

    @Override
    public void updateProject(String projectId, String name){
        Project project = getById(projectId);
        if (project == null || name == null || name.isEmpty()) return;
        project.setName(name);
    }

    @Override
    public boolean isEmptyProjectList() {
        return IProjectRepository.findAll().isEmpty();
    }

    @Override
    public Project getById(String id){
        if (id.isEmpty()) return null;
        return IProjectRepository.findOne(id);
    }

    @Override
    public void deleteProject(String projectId) {
        Project project = getById(projectId);
        if (project == null) return;
        IProjectRepository.remove(project);
    }

    @Override
    public void deleteAllProject() {
        IProjectRepository.removeAll();
    }

}

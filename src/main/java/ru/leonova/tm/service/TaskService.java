package ru.leonova.tm.service;

import ru.leonova.tm.api.repository.ITaskRepository;
import ru.leonova.tm.api.service.ITaskService;
import ru.leonova.tm.entity.Task;

import java.util.Collection;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    final private ITaskRepository ITaskRepository;

    public TaskService(ITaskRepository ITaskRepository) {
        this.ITaskRepository = ITaskRepository;
    }

    @Override
    public void create(Task t) {
        if (t == null) return;
        if (ITaskRepository.findAll().isEmpty()) {
            ITaskRepository.persist(t);
            return;
        }
        ITaskRepository.merge(t);
    }

    @Override
    public Collection<Task> getList() {
        return ITaskRepository.findAll();
    }

    @Override
    public void updateTaskName(String taskId, String taskName) {
        Task task = ITaskRepository.findOne(taskId);
        if (task == null || taskName == null || taskName.isEmpty()) return;
        task.setName(taskName);
    }

    @Override
    public void deleteTasksByIdProject(String projectId) {
        if (projectId.isEmpty()) return;
        Collection<Task> taskCollection = ITaskRepository.findAll();
        taskCollection.removeIf(task -> task.getProjectId().equals(projectId));
    }

    @Override
    public boolean isEmptyTaskList() {
        return ITaskRepository.findAll().isEmpty();
    }

    @Override
    public void deleteTask(String taskId) {
        Task task = ITaskRepository.findOne(taskId);
        if (task == null) return;
        ITaskRepository.remove(task);
    }

    @Override
    public Task getById(String id){
        return ITaskRepository.findOne(id);
    }

    @Override
    public void deleteAllTask() {
        ITaskRepository.removeAll();
    }

}

package ru.leonova.tm.service;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractService<E> {

    abstract void create(E e);
    abstract Collection<E> getList();
    abstract E getById(String eId);
}

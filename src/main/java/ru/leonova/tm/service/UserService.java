package ru.leonova.tm.service;

import org.apache.commons.codec.digest.DigestUtils;
import ru.leonova.tm.api.repository.IUserRepository;
import ru.leonova.tm.api.service.IUserService;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;

import java.util.Collection;

public final class UserService extends AbstractService<User> implements IUserService {

    final private IUserRepository IUserRepository;
    private User currentUser;

    public UserService(IUserRepository IUserRepository) {
        this.IUserRepository = IUserRepository;
    }
    @Override
    public User getCurrentUser() {
        return currentUser;
    }
    @Override
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public void create(User user) {
        if (user == null) return;
        user.setRoleType(RoleType.USER.getRole());
        IUserRepository.persist(user);
    }

    @Override
    public Collection<User> getList() {
        return IUserRepository.findAll();
    }

    @Override
    public User authorizationUser(String login, String password) {
        if (login.isEmpty() || password.isEmpty()) return null;
        Collection<User> userCollection = IUserRepository.findAll();
        for (User user : userCollection) {
            if (user.getLogin().equals(login) & user.getPassword().equals(md5Apache(password))) {
                currentUser = user;
                return currentUser;
            }
        }
        return null;
    }


    @Override
    public boolean isAuth() {
        return currentUser != null;
    }

    @Override
    public User getById(String userId) {
        return IUserRepository.findOne(userId);
    }

    @Override
    public String md5Apache(String password) {
        return DigestUtils.md5Hex(password);
    }


    @Override
    public void adminRegistration(User admin) {
        IUserRepository.persist(admin);
    }
}

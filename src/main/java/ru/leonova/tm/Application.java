package ru.leonova.tm;

import ru.leonova.tm.bootstrap.Bootstrap;
import ru.leonova.tm.command.project.*;
import ru.leonova.tm.command.system.AboutCommand;
import ru.leonova.tm.command.system.ExitCommand;
import ru.leonova.tm.command.system.HelpCommand;
import ru.leonova.tm.command.task.*;
import ru.leonova.tm.command.user.*;

public class Application {

    private static final Class[] classes = {HelpCommand.class, ProjectCreateCommand.class,
            ProjectDeleteCommand.class, ProjectShowListCommand.class, ProjectUpdateCommand.class, TaskCreateCommand.class,
            TaskUpdateNameCommand.class, TaskDeleteCommand.class, TaskDeleteAllOfProjectCommand.class, TaskDeleteListCommand.class,
            TaskShowListCommand.class, ProjectDeleteListCommand.class, ExitCommand.class, UserAuthorizationCommand.class, UserRegistrationCommand.class,
            UserUpdateRoleCommand.class, UserSessionEndCommand.class, UserUpdateLoginCommand.class, UserUpdatePasswordCommand.class, UserShowCommand.class,
            UserShowListCommand.class, AboutCommand.class};

    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(classes);
    }

}

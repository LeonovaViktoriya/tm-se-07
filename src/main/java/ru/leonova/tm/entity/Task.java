package ru.leonova.tm.entity;

import java.util.Date;
import java.util.UUID;

public final class Task {

    private String name;
    private String projectId;
    private String taskId;
    private String description;
    private Date dateStart;
    private Date dateEnd;

    {
        UUID.randomUUID();
    }

    public Task(String name, String prId) {
        this.projectId = prId;
        this.name = name;
        taskId = UUID.randomUUID().toString();
    }

    public String getName() {
        return name;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }
}


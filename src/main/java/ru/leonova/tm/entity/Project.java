package ru.leonova.tm.entity;

import java.util.Date;
import java.util.UUID;

public final class Project {

    private String name;
    private String projectId;
    private String description;
    private Date dateStart;
    private Date dateEnd;
    private String userId;

    {
        UUID.randomUUID();
    }

    public Project(String name) {
        this.name = name;
        projectId = UUID.randomUUID().toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrID() {
        return projectId;
    }

    public void setPrID(String prID) {
        this.projectId = prID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}

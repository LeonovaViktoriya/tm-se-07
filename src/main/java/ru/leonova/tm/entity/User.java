package ru.leonova.tm.entity;

import java.util.Collection;
import java.util.UUID;

public class User {

    private String login;
    private String password;
    private String userId;
    private Collection<Project> projectId;
    private String taskId;
    private String roleType;

    public User(String login, String password){
        this.login = login;
        this.password = password;
        this.userId = UUID.randomUUID().toString();
    }
    {
        UUID.randomUUID();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Collection<Project> getProjectId() {
        return projectId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }
}

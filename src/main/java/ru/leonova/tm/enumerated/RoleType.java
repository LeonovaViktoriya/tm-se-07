package ru.leonova.tm.enumerated;

public enum RoleType {

    USER("user"),
    ADMIN("admin");

    private String role;

    RoleType(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
